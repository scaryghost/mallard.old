DEPS := deps
DEPS_INCLUDE := $(DEPS)/include

VOLAUVENT_HOME := modules/Volauvent
APP_NAME := mallard_parser
CXXFLAGS := -std=c++14 -I$(DEPS_INCLUDE)
APP_TYPE ?= static_lib

CATCH2_SRCS := catch.hpp
CATCH2_DIR = $(DEPS_INCLUDE)/catch2

TEST_RULES := $(addprefix $(CATCH2_DIR)/, $(CATCH2_SRCS))

include $(VOLAUVENT_HOME)/common.mk

$(CATCH2_DIR)/catch.hpp: modules/Catch2/single_include/catch2/catch.hpp | $(CATCH2_DIR)
	cp $< $@

$(DEPS_INCLUDE)/%:
	mkdir -p $@