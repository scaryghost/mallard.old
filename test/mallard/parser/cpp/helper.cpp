#include "helper.h"

using namespace std;
using namespace mallard::parser;

void consume_string(shared_ptr<Parser> parser, const string& input) {
    for(char ch: input) {
        parser->next(ch);
    }
}