#include "catch2/catch.hpp"

#include "helper.h"
#include "mallard/parser/parsers.h"

#include <string>
#include <vector>

using namespace std;
using namespace mallard::parser;

TEST_CASE("valid integers", "[int_parser]" ) {
    const vector<string> inputs = {
        "0123456789", 
        "-0123456789", 
        "-0123456789\r\t\n"
    };

    const vector<string> expected_tokens = {
        "0123456789", 
        "-0123456789", 
        "-0123456789"
    };

    for(size_t i = 0; i < inputs.size(); i++) {
        for(auto parser: { integer_parser(), expression_parser() }) {
            consume_string(parser, inputs[i]);

            CHECK(parser->accepted_input());
            CHECK(parser->matches()[0].tokens[0].str() == expected_tokens[i]);
        }
    }
}

TEST_CASE("invalid integers", "[int_parser_invalid]" ) {
    const auto inputs = {
        "0.123456789", 
        "the quick brown fox", 
        "0123 4567 89"
    };

    for(auto& input: inputs) {
        CHECK_THROWS_AS(consume_string(integer_parser(), input), runtime_error);
    }
}

TEST_CASE("valid floats", "[float_parser]" ) {
    const vector<string> inputs = {
        "3.14159\t\r\n",
        "3.14159",
        "-3.14159",
        "-3.14159f",
        "-3.14159f\t\r\n",
        "3.14159f",
        "3f",
        "0f",
        "-0f"
    };

    const vector<vector<string>> expected_tokens = {
        {"3", "14159"},
        {"3", "14159"},
        {"-3", "14159"},
        {"-3", "14159"},
        {"-3", "14159"},
        {"3", "14159"},
        {"3", ""},
        {"0", ""},
        {"-0", ""}
    };

    for(size_t i = 0; i < inputs.size(); i++) {
        for(auto parser: { float_parser(), expression_parser() }) {            
            consume_string(parser, inputs[i]);

            auto& matches = parser->matches();
            CHECK(parser->accepted_input());
            CHECK(matches[0].tokens[0].str() == expected_tokens[i][0]);
            CHECK(matches[0].tokens[1].str() == expected_tokens[i][1]);
        }
    }
}

TEST_CASE("invalid floats", "[float_parser_invalid]" ) {
    const auto inputs = {
        ".14159", 
        "f", 
        "-.14159",
        "3.141.59"
    };

    for(auto& input: inputs) {
        CHECK_THROWS_AS(consume_string(float_parser(), input), runtime_error);
    }
}