#include "mallard/parser/parser.h"

#include <memory>
#include <string>

void consume_string(std::shared_ptr<mallard::parser::Parser> parser, const std::string& input);