#include "catch2/catch.hpp"

#include "helper.h"
#include "mallard/parser/parsers.h"

using namespace std;
using namespace mallard::parser;

TEST_CASE("assign float", "[val_float_parser]" ) {
    const vector<string> inputs = {
        "val pi = 3.14159",
        "val pi = 3.14159\t\r\n",
        "val pi=3.14159",
        "val\npi\t=\r3.14159"
    };

    for (const auto& input: inputs) {
        auto parser = val_parser();
        consume_string(parser, input);

        CHECK(parser->accepted_input());
        CHECK(parser->matches()[0].tokens[0].str() == "pi");
        CHECK(parser->matches()[1].tokens[0].str() == "3");
        CHECK(parser->matches()[1].tokens[1].str() == "14159");
    }
}