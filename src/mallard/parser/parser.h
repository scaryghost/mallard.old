#pragma once

#include <functional>
#include <memory>
#include <sstream>
#include <vector>

namespace mallard {
namespace parser {

enum TokenType {
    INTEGER,
    FLOAT,
    IDENTIFIER
};

struct Match {
    std::vector<std::stringstream> tokens;
    TokenType type;
};

struct Parser {
    virtual ~Parser();

    virtual void next(char ch) = 0;
    virtual bool accepted_input() const = 0;
    virtual std::vector<Match>& matches() = 0;
};

}
}