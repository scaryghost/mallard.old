#include "mallard/parser/parsers.h"

#include "composed_parser.h"
#include "dfa_parser.h"
#include "dfa_parser_builder.h"

#include <initializer_list>
#include <memory>

using namespace std;

namespace mallard {
namespace parser {

static const vector<State> VAL_STATES = {
    {{{ 1, no_group, IS_CHAR('v') }}, ""},
    {{{ 2, no_group, IS_CHAR('a') }}, ""},
    {{{ 3, no_group, IS_CHAR('l') }}, ""},
    {{{ 4, no_group, IS_SPACE }}, ""},
    {{{ 4, no_group, IS_SPACE }}, ""}
}, EQUAL_STATES = {
    {{{ 0, no_group, IS_SPACE },  { 1, no_group, IS_CHAR('=') }}, ""},
    {{{ 1, no_group, IS_SPACE }}, "Only space characters allowed after '='"}
};


shared_ptr<Parser> val_parser() {
    initializer_list<shared_ptr<Parser>> parsers = {
        make_shared<DFAParser>(VAL_STATES, unordered_set<size_t>{ 4 }),
        identifier_parser(),
        make_shared<DFAParser>(EQUAL_STATES, unordered_set<size_t>{ 1 }),
        expression_parser()
    };
    return make_shared<ComposedParser>(parsers);
}

}
}
