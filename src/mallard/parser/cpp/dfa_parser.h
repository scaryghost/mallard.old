#pragma once

#include "mallard/parser/parser.h"

#include <cstddef>
#include <sstream>
#include <unordered_set>
#include <vector>

namespace mallard {
namespace parser {

const std::size_t no_group = -1;

struct Transition {
    std::size_t next;
    std::size_t group;
    bool(*criteria)(char);
};

struct State {
    std::vector<Transition> transitions;
    std::string error_msg;
};

struct DFAParser : public Parser {
    DFAParser(
        const std::vector<State>& states, 
        const std::unordered_set<std::size_t>& accepting_states
    );
    virtual ~DFAParser();

    virtual void next(char ch);
    virtual bool accepted_input() const {
        return accepting_states.count(current);
    }
    virtual std::vector<Match>& matches();

private:
    std::vector<Match> _matches;
    const std::vector<State>& states;
    std::unordered_set<size_t> accepting_states;
    std::size_t current;
};

}
}