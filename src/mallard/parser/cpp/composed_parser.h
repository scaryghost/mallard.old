#pragma once

#include "mallard/parser/parser.h"

#include <initializer_list>
#include <vector>

namespace mallard {
namespace parser {

struct ComposedParser : public Parser {
    ComposedParser(const std::initializer_list<std::shared_ptr<Parser>>& parsers);
    virtual ~ComposedParser();

    virtual void next(char ch);
    virtual bool accepted_input() const;
    virtual std::vector<Match>& matches();

private:
    std::vector<std::shared_ptr<Parser>> parsers;
    std::vector<std::shared_ptr<Parser>>::iterator parser_iterator;
    std::vector<Match> _matches;
};

}
}