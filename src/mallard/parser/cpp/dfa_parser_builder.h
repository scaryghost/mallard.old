#pragma once

#define CRITERIA(p) [](char ch) -> bool { return p; }
#define IS_DIGIT CRITERIA(isdigit(ch))
#define IS_CHAR(v) CRITERIA(ch == v)
#define IS_SPACE CRITERIA(isspace(ch))