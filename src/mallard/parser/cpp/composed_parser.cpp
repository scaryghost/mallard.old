#include "composed_parser.h"

#include <iterator>

using namespace std;

namespace mallard {
namespace parser {

ComposedParser::ComposedParser(const initializer_list<shared_ptr<Parser>>& parsers) : 
    parsers(parsers),
    parser_iterator(this->parsers.begin())
{

}

ComposedParser::~ComposedParser() {

}

void ComposedParser::next(char ch) {
    auto current = *parser_iterator;

    try {
        current->next(ch);
    } catch (const runtime_error& e) {
        if (current->accepted_input()) {
            parser_iterator++;
            next(ch);
        } else {
            throw e;
        }
    }
}

bool ComposedParser::accepted_input() const {
    auto last = parsers.end();
    last--;

    return parser_iterator == last && (*parser_iterator)->accepted_input();
}

vector<Match>& ComposedParser::matches() {
    for(auto parser: parsers) {
        auto& matches = parser->matches();
        _matches.insert(_matches.end(), make_move_iterator(matches.begin()), make_move_iterator(matches.end()));
    }

    return _matches;
}

}
}