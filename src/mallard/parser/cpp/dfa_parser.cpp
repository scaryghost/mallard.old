#include "dfa_parser.h"

#include <algorithm>
#include <stdexcept>

using namespace std;

namespace mallard {
namespace parser {

DFAParser::DFAParser(
    const vector<State>& states, 
    const unordered_set<size_t>& accepting_states
) : 
    states(states),
    accepting_states(accepting_states),
    current(0)
{

}

DFAParser::~DFAParser() {

}

void DFAParser::next(char ch) {
    const auto& state = states.at(current);
    auto end = state.transitions.end();
    auto transition = find_if(state.transitions.begin(), end, [ch](const Transition& t) {
        return t.criteria(ch);
    });

    if (transition == end) {
        throw runtime_error(state.error_msg);
    }

    current = transition->next;
    if (transition->group != no_group) {
        _matches[0].tokens[transition->group] << ch;
    }
}

vector<Match>& DFAParser::matches() {
    return _matches;
}

}
}