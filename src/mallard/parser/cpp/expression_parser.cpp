#include "mallard/parser/parsers.h"

#include <algorithm>

using namespace std;

namespace mallard {
namespace parser {

struct ExpressionParser : public Parser {
    ExpressionParser();
    virtual ~ExpressionParser();

    virtual void next(char ch);
    virtual bool accepted_input() const;
    virtual vector<Match>& matches() {
        return possible[0]->matches();
    }

private:
    vector<shared_ptr<Parser>> possible;
};

ExpressionParser::ExpressionParser() :
    possible({ float_parser(), integer_parser(), identifier_parser() })
{
        
}

ExpressionParser::~ExpressionParser() {

}

void ExpressionParser::next(char ch) {
    vector<shared_ptr<Parser>> still_possible;

    copy_if(possible.begin(), possible.end(), back_inserter(still_possible), [ch](const shared_ptr<Parser>& parser) {
        try {
            parser->next(ch);
            return true;
        } catch (const runtime_error&) {
            return false;
        }
    });

    possible = still_possible;

    if (still_possible.empty()) {
        throw runtime_error("invalid expression");
    }    
}

bool ExpressionParser::accepted_input() const {
    vector<shared_ptr<Parser>> still_possible;

    copy_if(possible.begin(), possible.end(), back_inserter(still_possible), [](const shared_ptr<Parser>& parser) {
        return parser ->accepted_input();
    });

    return still_possible.size() == 1;
}

shared_ptr<Parser> expression_parser() {
    return make_shared<ExpressionParser>();
}

}
}