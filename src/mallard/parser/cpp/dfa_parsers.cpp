#include "mallard/parser/parsers.h"

#include "dfa_parser.h"
#include "dfa_parser_builder.h"

using namespace std;

namespace mallard {
namespace parser {

static const vector<State> FLOAT_STATES = {
    {{{ 1, 0, IS_CHAR('-') }, { 2, 0, IS_DIGIT }}, "float must begin with a digit or '-'"},
    {{{ 2, 0, IS_DIGIT }}, "digits must come after a '-' character"},
    {{{ 2, 0, IS_DIGIT }, {3, no_group, IS_CHAR('.') }, {5, no_group, IS_CHAR('f') }}, "Only digits, '.', or 'f' can come after a digit"},
    {{{ 4, 1, IS_DIGIT }}, "Only digits can come immediately after '.'"},
    {{{ 4, 1, IS_DIGIT }, {5, no_group, IS_CHAR('f') }, {5, no_group, IS_SPACE }}, "Only digits or 'f' can come after '.'"},
    {{{ 5, no_group, IS_SPACE }}, "No more characters allowed after 'f'"}
};
static const unordered_set<size_t> ACCEPTING_FLOAT_INDICES = {4, 5};

const char* IDENTIFIER_CHARS_MSG = "identifier can only contain [a-zA-Z0-9_] characters";
static const vector<State> IDENTIFIER_STATES = {
    {{{ 1, 0, IS_CHAR('v') }, { 4, 0, CRITERIA(ch != 'v' && (isalpha(ch) || ch == '_')) }}, "identifier must begin with a letter or '_'"},
    {{{ 2, 0, IS_CHAR('a') }, { 4, 0, CRITERIA(ch != 'a' && (isalnum(ch) || ch == '_')) }}, IDENTIFIER_CHARS_MSG},
    {{{ 3, 0, IS_CHAR('l') }, { 4, 0, CRITERIA(ch != 'l' && (isalnum(ch) || ch == '_')) }}, IDENTIFIER_CHARS_MSG},
    {{{ 4, 0, CRITERIA(isalnum(ch) || ch == '_') }}, "'val' is not a valid identifier"},
    {{{ 4, 0, CRITERIA(isalnum(ch) || ch == '_') }, { 5, no_group, IS_SPACE }}, IDENTIFIER_CHARS_MSG},
    {{{ 5, no_group, IS_SPACE }}, "No characters allowed after space"}
};
static const unordered_set<size_t> ACCEPTING_IDENTIFIER_INDICES = {4, 5};

static const vector<State> INT_STATES = {
    {{{ 1, 0, IS_CHAR('-') }, { 2, 0, IS_DIGIT }}, "integer must begin with a digit or '-'"},
    {{{ 2, 0, IS_DIGIT }}, "digits must come after a '-' character"},
    {{{ 2, 0, IS_DIGIT }, { 3, no_group, IS_SPACE }}, "Only digits can come after a digit"},
    {{{ 3, no_group, IS_SPACE }}, "No characters allowed after space"}
};
static const unordered_set<size_t> ACCEPTING_INT_INDICES = {2, 3};

shared_ptr<Parser> float_parser() {
    auto parser = make_shared<DFAParser>(FLOAT_STATES, ACCEPTING_FLOAT_INDICES);

    auto& matches = parser->matches();
    matches.resize(1);
    matches[0].tokens.resize(2);
    matches[0].type = FLOAT;

    return parser;
}

shared_ptr<Parser> identifier_parser() {
    auto parser = make_shared<DFAParser>(IDENTIFIER_STATES, ACCEPTING_IDENTIFIER_INDICES);

    auto& matches = parser->matches();
    matches.resize(1);
    matches[0].tokens.resize(1);
    matches[0].type = IDENTIFIER;

    return parser;
}

shared_ptr<Parser> integer_parser() {
    auto parser = make_shared<DFAParser>(INT_STATES, ACCEPTING_INT_INDICES);
    
    auto& matches = parser->matches();
    matches.resize(1);
    matches[0].tokens.resize(1);
    matches[0].type = INTEGER;

    return parser;
}

}
}