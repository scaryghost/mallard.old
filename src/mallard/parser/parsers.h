#pragma once

#include "parser.h"

#include <memory>

namespace mallard {
namespace parser {

std::shared_ptr<Parser> integer_parser();
std::shared_ptr<Parser> float_parser();
std::shared_ptr<Parser> identifier_parser();

std::shared_ptr<Parser> expression_parser();
std::shared_ptr<Parser> val_parser();
}
}